require 'NCBI'

def get_feature str
  ft = {}
  str.split(";").each do |term|
    key, val = term.split "="
    next if key.eql? 'Dbxref'
    ft[key] = val
  end
  return ft
end

def usage
  puts ""
  puts "USAGE ruby gff2gtf.rb <dbname> <gff_file>"
  puts ""
  Process.exit 1
end

ercc  = '/POC/NGS/bings/genomes/markers/ERCC.gtf'

logger = NCBI::Logger.new STDOUT
logger.info "starting ..."

usage() unless ARGV.length == 2

dbname  = ARGV[0]
gffname = ARGV[1]

unless File.exists? gffname
  logger.fatal "the file of #{gffname} doesn't exist"
  Process.exit 1
end

begin
  Database::connect2db dbname
rescue Excpetion => e
  logger.warn e.message
  Process.exit 1
end

begin
  db = Database::Genome.find 1
rescue Exception => e
  logger.fatal e.message
  Process.exit 1
end

if db.status > 5
  logger.info "already done. skipped"
  Process.exit 1
end

logger.info "fetching info of chromosomes and contigs ..."
chrs = {}
Database::Contig.all.each { |ctg| chrs[ctg.accession] = ctg.chromosome }

GFFDir = File.join db.source, "GFF"
wkdir = db.wkdir 
dirs = ['anno', 'rsem', 'seq', 'star']
dirs.each { |name| NCBI.mkdir wkdir, name }

anno = File.join wkdir, 'anno'
gff  = File.join(anno, File.basename(gffname))

logger.info "copying NCBI GFF files ..."
begin
  system "cp #{gffname} #{gff}" unless File.exists?gff
  system "gunzip #{gff}"
rescue Exception => e
  logger.fatal e.message
  Process.exit 1
end

logger.info "unzipping the GFF file ..."
gff = gff.sub(/\.gz/, '')
unless File.exists? gff
  logger.fatal "couldn't find the gff file -> #{gff}"
  Process.exit 1
end

tmpGFF = File.join anno, "tmp.gff"
File.delete tmpGFF if File.exists? tmpGFF

ouFile = File.open tmpGFF, "w"
inFile = File.open gff
while line = inFile.gets
  term = line.chomp.split "\t"
  next unless chrs.has_key?term[0]
  term[0] = chrs[term[0]]
  ouFile.puts term.join "\t"
end
inFile.close
ouFile.close

tmpGTF = File.join(anno, "tmp.gtf")
logger.info "running gffread app ... "
begin
  system "gffread #{tmpGFF} -T -o #{tmpGTF}"
rescue Exception => e
  logger.fatal e.message
  logger.fatal "module add cufflinks?"
  Process.exit 1
end

logger.info "converting gene ids ..."
final = File.join(anno, "#{db.assembly_name}.gtf")
gf = File.open(final, "w")
fh = File.open tmpGTF
while line = fh.gets
  line = line.sub(/gene_id/, '~nil~')
  line = line.sub(/gene_name/, 'gene_id')
  line = line.sub(/~nil~/, 'gene_name')
  gf.puts line
end
fh.close
gf.close

logger.info "appending ERCC annotation data ..."
begin
  system "cat #{ercc} >> #{final}"
rescue Exception => e
  logger.info e.message
  Process.exit 1
end
db.update_attribute :status, 6

f = Database::File.new
f.genome_id = db.id
f.file_type = 'GTF'
f.name = final
f.save

File.delete tmpGFF
File.delete tmpGTF
logger.info "DONE"

