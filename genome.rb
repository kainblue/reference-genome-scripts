module NCBI
  class Genome
    attr_accessor :wkdir, :error, :genome

    def initialize source
      @source     = source
      self.error  = nil
      self.genome = nil
    end

    def has_seq? dest
      source = File.join @source, "Assembled_chromosomes/seq"
      @fasta = {} 

      Find.find(source) do |path|
        next if FileTest.directory?(path)
        chr = get_chr path
        next unless chr.include?'chr'
        @fasta[chr] = copy(path, dest)
      end

      if @fasta.length > 0
        return true
      else
        return false
      end
    end

    def decompress dest
      begin
        system("gunzip #{dest}/*.gz")
      rescue Exception => e
        self.error = e.message
      end
    end

    def get_chromosome flname
      base = File.basename flname, ".fa"
      term = base.split "_"
      chr  = term.pop
      ids  = []
      fh = open("| grep \">\" #{flname}")
      while line = fh.gets
        term = line.chomp.split "|"
        ids.push term[3] 
      end
      return chr, ids
    end

    def add_contig chr, gi, acc
      ctg = Database::Contig.new
      ctg.id = gi
      ctg.accession = acc
      ctg.chromosome = chr
      ctg.save
    end

    def create_genome dest
      tmpFile = File.join dest, "tmp.fa"
      File.delete tmpFile if File.exists? tmpFile

      files = {} 
      accession = {}

      Find.find(dest).each do |file|
        next unless file.end_with?'.fa'
        chr, ids = get_chromosome file
        files[chr] = file
        ids.each { |id| accession[id] = chr }
      end
      
      chrs = []
      1.upto(50) { |i| chrs.push "chr#{i}" }
      chrs.push "chrX"
      chrs.push "chrY"
      chrs.push "chrMT"

      total = []
      chrs.each { |chr| total.push files[chr] if files.has_key?chr }
      tmpFile = File.join dest, "tmp.fa"
      
      begin
        str = total.join " "
        system "cat #{str} > #{tmpFile}"
      rescue Exception => e
        self.error = e.message
        return
      end

      self.genome = File.join dest, "genome.fa"
      outFile = File.open self.genome, "w"
      inFile  = File.open tmpFile
      while line = inFile.gets
        line = line.chomp
        if line.start_with?">"
          id, str2 = line.split " ", 2
          ig1, gi, ig2, acc = id.split "|"
          chr = accession[acc]
          line = ">#{chr} #{str2}"
          add_contig chr, gi, acc
        end
        outFile.puts line
      end
      inFile.close
      outFile.close
      File.delete tmpFile
      total.each {|f| File.delete f}
    end

    def add_ercc ercc
      begin
        system "cat #{ercc} >> #{self.genome}"
      rescue Exception => e
        self.error e.message
      end
      f = Database::File.new
      f.genome_id = 1
      f.file_type = 'genome'
      f.name = self.genome
      f.save
    end

    def get_chr file
      name = File.basename file, ".fa.gz"
      term = name.split "_"
      return term.pop
    end

    def copy oriFile, dst
      name = File.basename oriFile
      dest = File.join dst, name
      begin
        system("cp #{oriFile} #{dest}")
      rescue Exception => e
        puts e.message
        Process.exit 1
      end
      return dest
    end
  end
end

