require 'NCBI'

source = "/POC/NGS/bings/NCBI/"

species = { 'human'   => 'Homo_sapiens',
            'mouse'   => 'Mus_musculus',
            'rhesus'  => 'Macaca_mulatta',
            'macaca'  => 'Macaca_fascicularis'
           }

def usage 
  puts ""
  puts "USAGE ruby wget.rb <dbname> <species>"
  puts ""
  Process.exit 1
end

logger = NCBI::Logger.new STDOUT
logger.info "starting ..."

usage() unless ARGV.length == 2

dbname = ARGV[0]
spname = ARGV[1]

unless species.has_key? spname
  logger.warn "wrong species name? (human or mouse)"
  Process.exit 1
end

dirname = [ 'Assembled_chromosomes/seq/*.fa.gz', 
            'GFF/*.gz', 'README_CURRENT_RELEASE' ]

begin
  File.delete dbname if File.exists? dbname
rescue Exception => e
  puts e.message
  Process.exit 1
end

begin
  Database::connect2db dbname
  Database::add_table
rescue Exception => e
  logger.warn e.message
  Process.exit 1
end

remote = 'ftp://ftp.ncbi.nlm.nih.gov/genomes'
remote = File.join remote, species[spname]
destin = "--directory-prefix=#{source}"

dirname.each do |name|
  loc = File.join remote, name
  reject = nil
  if name.eql?'GFF'
    reject = '--reject=*alt*'
  elsif name.start_with? 'Assembled'
    reject = '--reject=*alt*,*.mfa.gz'
  else
    reject = ''
  end

  command = ['wget', '-r --continue', reject, destin, loc]
  cmd = command.join " "
  fh = open("| #{cmd}")
  while line = line
    puts line
  end
  fh.close
end

logger.info "done fetching data from NCBI"

src = File.join source, "ftp.ncbi.nlm.nih.gov/genomes/#{species[spname]}"
doc = File.join src,  "README_CURRENT_RELEASE"

if File.exists? doc
  genome = Database::Genome.new
  genome.name = spname

  fh = File.open doc
  while line = fh.gets
    key, val = line.chomp.split ":\t",2
    if key.eql? 'ORGANISM NAME'
      genome.species = val.strip
    elsif key.eql? 'TAXID'
      genome.taxonomy = val.strip
    elsif key.eql? 'ASSEMBLY NAME'
      genome.assembly_name= val.strip
    elsif key.eql? 'ASSEMBLY DATE'
      genome.assembly_at = val.strip
    end
    if genome.species and genome.taxonomy and 
      genome.assembly_name and genome.assembly_at
      break
    end
  end
  fh.close

  genome.source = src
  genome.status = 1
  genome.save
else
  logger.info "please add genome info to the genomes table, before going to next"
end

logger.info "done"

