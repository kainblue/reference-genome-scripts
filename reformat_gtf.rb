def swapt str
  term = str.split "; "
  if term[0].include? 'gene_id' and term[2].include?'gene_name'
    term[0] = term[0].sub(/gene_id/,   'gene_name')
    term[2] = term[2].sub(/gene_name/, 'gene_id')
    temp = term[0]
    term[0] = term[2]
    term[2] = temp
  else
    puts "Oops"
    return nil
  end
  return term.join "; "
end

fh = File.open ARGV[0]
while line = fh.gets
  line = line.chomp
  term = line.split "\t"
  rtv  = swapt term[8]
  if rtv
    term[8] = rtv
    puts term.join "\t"
  end
end
fh.close

